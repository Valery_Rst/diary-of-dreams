package dreams.controllers.logged;

import animatefx.animation.Pulse;
import dreams.utils.*;
import dreams.alerts.ConfirmDialog;
import dreams.alerts.InfoDialog;
import dreams.database.DatabaseHandler;
import dreams.database.DreamerHandler;
import dreams.database.UserHandler;
import dreams.errors.PutErrorsInDialog;
import dreams.model.User;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class MainContentSidebarController implements Keys {

    @FXML
    private Pane home;

    @FXML
    private Pane add;

    @FXML
    private Pane show;

    @FXML
    private Pane delete;

    @FXML
    private Pane exit;

    @FXML
    private Pane deleteProfile;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private AnchorPane rightColumnAnchorPane;

    private User user;

    private DatabaseHandler databaseHandler = new DatabaseHandler();
    private DreamerHandler dreamerHandler;

    @FXML
    private void initialize(){
        databaseHandler.connecting();
        user = BinderUser.getInstance().getUser();

        if (hasUnknownUser()) {
            return;
        }

        new ChangerContainer(rightColumnAnchorPane, getClass().getResource(FXML_MAIN_CONTENT_RIGHT)).setLocation(500);
        new Pulse(anchorPane).play();
    }

    @FXML
    private void handleDeleteUser() {
        ConfirmDialog confirmDialog = new ConfirmDialog(new Label("Подтверждение"),
                new Text(" Вы действительно собираетесь удалить профиль?\n\n " +
                        "В случае подтверждения аккаунт будет безвозвратно потерян.\n"),
                anchorPane);
        confirmDialog.showDialog();
        confirmDialog.getAcceptButton().setOnMouseClicked(event -> {
            UserHandler userHandler = new UserHandler(databaseHandler.getConnection(), this.user);
            userHandler.deleteUser();
            dreamerHandler = new DreamerHandler(databaseHandler.getConnection(), this.user);
            dreamerHandler.deleteAllUserDreams();
            BinderUser.getInstance().setUnknownBinderUser();
            new InfoDialog(new Label("Успешно"), new Text("Вы успешно удалили профиль"), anchorPane).showDialog();
            new ChangerContainer(anchorPane, getClass().getResource(FXML_LAUNCH)).setLocation(1000);
        });
        confirmDialog.getCloseButton().setOnMouseClicked(event -> confirmDialog.closeDialog());
        databaseHandler.close();
    }

    @FXML
    private void handleSignOutUser() {
        ConfirmDialog confirmDialog = new ConfirmDialog(new Label("Подтверждение"),
                new Text("Вы действительно собираетесь выйти из профиля?"), anchorPane);
        confirmDialog.showDialog();
        confirmDialog.getAcceptButton().setOnMouseClicked(event -> {
            BinderUser.getInstance().setUnknownBinderUser();
            new InfoDialog(new Label("Успешно"), new Text("Вы успешно вышли из профиля"), anchorPane).showDialog();
            new ChangerContainer(anchorPane, getClass().getResource(FXML_LAUNCH)).setLocation(1000);
        });
        confirmDialog.getCloseButton().setOnMouseClicked(event -> confirmDialog.closeDialog());
        databaseHandler.close();
    }

    @FXML
    private void handleAddDreamWindow() {
        new ChangerContainer(rightColumnAnchorPane, getClass().getResource(FXML_ADD_DREAM)).setLocation(500);
    }

    @FXML
    private void handleShowDream() {
        dreamerHandler = new DreamerHandler(databaseHandler.getConnection(), this.user);
        if (dreamerHandler.getCountOfEntries() < 1) {
            PutErrorsInDialog putErrorsInDialog = new PutErrorsInDialog(anchorPane);
            putErrorsInDialog.setError(" В таблице пока не содержится ни одного сноведения\n " +
                    "Перейдите на окно добавления сна, чтобы записать сноведение.");
            putErrorsInDialog.showError(new Label("Предупреждение"));
        } else new ChangerContainer(rightColumnAnchorPane, getClass().getResource(FXML_SHOW_DREAM)).setLocation(500);
        databaseHandler.close();
    }

    @FXML
    private void handleDeleteDream() {
        dreamerHandler = new DreamerHandler(databaseHandler.getConnection(), this.user);
        if (dreamerHandler.getCountOfEntries() < 1) {
            PutErrorsInDialog putErrorsInDialog = new PutErrorsInDialog(anchorPane);
            putErrorsInDialog.setError(" В таблице пока не содержится ни одного сноведения\n " +
                    "Перейдите на окно добавления сна, чтобы записать сноведение.");
            putErrorsInDialog.showError(new Label("Предупреждение"));
        } else new ChangerContainer(rightColumnAnchorPane, getClass().getResource(FXML_DELETE_DREAM)).setLocation(500);
        databaseHandler.close();
    }

    @FXML
    private void handleMainWindow() {
        new ChangerContainer(anchorPane, getClass().getResource(FXML_MAIN_CONTENT)).setLocation(500);
    }

    private boolean hasUnknownUser() {
        if (BinderUser.getInstance().isUnknownBinderUser()) {
            add.setDisable(true);
            home.setDisable(true);
            show.setDisable(true);
            delete.setDisable(true);
            exit.setDisable(true);
            deleteProfile.setDisable(true);
            BinderContainer.getInstance().setAnchor(anchorPane);
            new InfoDialog(
                    new Label("Ошибка!"), new Text("Неизвестный пользователь. Пожалуйста, авторизируйтесь."), BinderContainer.getInstance().getAnchor()).showDialogWithTimer();
            return true;
        }
        BinderContainer.getInstance().resetAnchorPane();
        return false;
    }
}