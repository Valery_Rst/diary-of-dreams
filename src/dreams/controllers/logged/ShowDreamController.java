package dreams.controllers.logged;

import animatefx.animation.BounceInDown;
import animatefx.animation.Pulse;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import dreams.utils.*;
import dreams.alerts.ConfirmDialog;
import dreams.alerts.InfoDialog;
import dreams.database.DatabaseHandler;
import dreams.database.DreamerHandler;
import dreams.errors.AddDreamErrorsHandler;
import dreams.model.*;
import javafx.fxml.FXML;

import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class ShowDreamController implements Keys {

    @FXML
    private JFXTreeTableView<Dream> treeTable;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private JFXButton updateButton;

    @FXML
    private Pane textPane;

    private User user;
    private DatabaseHandler databaseHandler = new DatabaseHandler();
    private TreeTableColumns treeTableColumns;

    @FXML
    private void initialize() {
        new Pulse(anchorPane).play();
        new BounceInDown(textPane).play();
        new BounceInDown(treeTable).play();
        databaseHandler.connecting();
        user = BinderUser.getInstance().getUser();

        if (hasUnknownUser()) {
            return;
        }

        treeTable.setPlaceholder(new Label("Не найдено"));
        treeTableColumns = new TreeTableColumns();
        treeTableColumns.setViewTableColumns();
        treeTableColumns.setEditTableColumns();
        treeTableColumns.setSearchView(treeTable, anchorPane);
        treeTableColumns.initData();

        RecursiveTreeItem<Dream> root = new RecursiveTreeItem<>(treeTableColumns.getObservableList(), RecursiveTreeObject::getChildren);
        treeTable.getColumns().setAll(treeTableColumns.getDreamNumber(), treeTableColumns.getDream(), treeTableColumns.getDreamType(),
                treeTableColumns.getDreamHour(), treeTableColumns.getDreamDate(), treeTableColumns.getDreamTime());
        treeTable.setRoot(root);
        treeTable.setEditable(true);
        treeTable.setShowRoot(false);
    }

    public void handleUpdateDreamButton() {
        int selectedIndex = treeTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            ConfirmDialog confirmDialog = new ConfirmDialog(new Label("Подтверждение"), new Text("Вы собираетесь обновить содержимое сна?"), anchorPane);
            confirmDialog.showDialog();
            confirmDialog.getAcceptButton().setOnAction(event -> {

                AddDreamErrorsHandler addDreamErrorsHandler = new AddDreamErrorsHandler();

                String editDream = treeTableColumns.getValue(treeTable.getTreeItem(selectedIndex).getValue().getDream(), treeTableColumns.getEditDream());
                String editDreamType = treeTableColumns.getValue(treeTable.getTreeItem(selectedIndex).getValue().getDreamType(), treeTableColumns.getEditType());
                String editDreamHour = treeTableColumns.getValue(treeTable.getTreeItem(selectedIndex).getValue().getDreamHour(), treeTableColumns.getEditHour());
                String editNumber = String.valueOf(treeTableColumns.getValue(treeTable.getTreeItem(selectedIndex).getValue().getCountEntries(), treeTableColumns.getEditNumber()));

                if (addDreamErrorsHandler.isEmptyField(editDream) | addDreamErrorsHandler.isEmptyField(editDreamType) | addDreamErrorsHandler.isEmptyField(editDreamHour)
                        | addDreamErrorsHandler.isEmptyField(editNumber)) {
                    new InfoDialog(new Label("Предупреждение"), new Text("Поле не должно быть пустым"), anchorPane).showDialog();
                    confirmDialog.closeDialog();
                } else {
                    Dream dream = new Dream(
                            treeTable.getTreeItem(selectedIndex).getValue().getUserId(),
                            editDream, editDreamType, editDreamHour,
                            treeTable.getTreeItem(selectedIndex).getValue().getDate(), treeTable.getTreeItem(selectedIndex).getValue().getTime(),
                            Integer.parseInt(editNumber));
                    DreamerHandler dreamerHandler = new DreamerHandler(databaseHandler.getConnection(), user);
                    dreamerHandler.updateDream(dream);
                    treeTableColumns.refresh();
                    confirmDialog.closeDialog();
                    new InfoDialog(new Label("Успешно"), new Text("Сон успешно обновлён"), anchorPane).showDialog();
                    new ChangerContainer(anchorPane, getClass().getResource(FXML_MAIN_CONTENT_RIGHT)).setLocation(1000);
                }
            });
            confirmDialog.getCloseButton().setOnAction(event -> confirmDialog.closeDialog());
        } else {
            InfoDialog infoDialog = new InfoDialog(new Label("Предупреждение"), new Text("Выберите нужный сон для обновления"), anchorPane);
            infoDialog.showDialog();
        }
        databaseHandler.close();
    }

    private boolean hasUnknownUser() {
        if (BinderUser.getInstance().isUnknownBinderUser()) {
            updateButton.setDisable(true);
            BinderContainer.getInstance().setAnchor(anchorPane);
            new InfoDialog(
                    new Label("Ошибка!"), new Text("Неизвестный пользователь. Пожалуйста, авторизируйтесь."), BinderContainer.getInstance().getAnchor()).showDialogWithTimer();
            return true;
        }
        BinderContainer.getInstance().resetAnchorPane();
        return false;
    }
}