package dreams.controllers.logged;


import animatefx.animation.BounceInDown;
import animatefx.animation.Pulse;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import dreams.utils.*;
import dreams.alerts.ConfirmDialog;
import dreams.alerts.InfoDialog;
import dreams.database.DatabaseHandler;
import dreams.database.DreamerHandler;
import dreams.model.Dream;
import dreams.model.User;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class DeleteDreamController implements Keys {

    @FXML
    private JFXTreeTableView<Dream> treeTable;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private JFXButton deleteButton;

    @FXML
    private Pane textPane;

    private User user;
    private DatabaseHandler databaseHandler = new DatabaseHandler();
    private TreeTableColumns treeTableColumns;

    @FXML
    private void initialize() {
        new Pulse(anchorPane).play();
        new BounceInDown(textPane).play();
        user = BinderUser.getInstance().getUser();
        databaseHandler.connecting();

        if (hasUnknownUser()) {
            return;
        }

        treeTableColumns = new TreeTableColumns();
        treeTableColumns.setViewTableColumns();

        treeTableColumns.initData();
        RecursiveTreeItem<Dream> root = new RecursiveTreeItem<>(treeTableColumns.getObservableList(), RecursiveTreeObject::getChildren);
        treeTable.getColumns().setAll(treeTableColumns.getDreamNumber(), treeTableColumns.getDream(), treeTableColumns.getDreamType(),
                treeTableColumns.getDreamHour(), treeTableColumns.getDreamDate(), treeTableColumns.getDreamTime());
        treeTable.setRoot(root);
        treeTable.setEditable(true);
        treeTable.setShowRoot(false);
    }

    @FXML
    private void handleDeleteDreamButton() {
        int selectedIndex = treeTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    new Label("Подтверждение удаления"),
                    new Text("Вы действительно собираетесь удалить выбранный сон? \n" +
                            "Подтвердив это действие, Вы безвозвратно его удалите."),
                    anchorPane);
            confirmDialog.showDialog();
            confirmDialog.getAcceptButton().setOnAction(event -> {
                DreamerHandler dreamerHandler = new DreamerHandler(databaseHandler.getConnection(), user);
                dreamerHandler.deleteSelectUserDream(treeTable.getTreeItem(selectedIndex).getValue());
                treeTableColumns.refresh();
                confirmDialog.closeDialog();
                new InfoDialog(new Label("Успешно"), new Text("Сон успешно удалён"), anchorPane).showDialog();
                new ChangerContainer(anchorPane, getClass().getResource(FXML_MAIN_CONTENT_RIGHT)).setLocation(1000);
            });
            confirmDialog.getCloseButton().setOnAction(event -> confirmDialog.closeDialog());
        } else {
            InfoDialog infoDialog = new InfoDialog(new Label("Предупреждение"), new Text("Выберите нужный сон для удаления"), anchorPane);
            infoDialog.showDialog();
        }
        databaseHandler.close();
    }


    private boolean hasUnknownUser() {
        if (BinderUser.getInstance().isUnknownBinderUser()) {
            deleteButton.setDisable(true);
            BinderContainer.getInstance().setAnchor(anchorPane);
            new InfoDialog(
                    new Label("Ошибка!"), new Text("Неизвестный пользователь. Пожалуйста, авторизируйтесь."), BinderContainer.getInstance().getAnchor()).showDialogWithTimer();
            return true;
        }
        BinderContainer.getInstance().resetAnchorPane();
        return false;
    }
}