package dreams.controllers.logged;

import animatefx.animation.BounceInDown;
import animatefx.animation.Pulse;
import com.jfoenix.controls.*;
import dreams.utils.*;
import dreams.alerts.ConfirmDialog;
import dreams.alerts.InfoDialog;
import dreams.database.DatabaseHandler;
import dreams.database.DreamHourHandler;
import dreams.database.DreamTypeHandler;
import dreams.database.DreamerHandler;
import dreams.errors.AddDreamErrorsHandler;
import dreams.errors.PutErrorsInDialog;
import dreams.model.*;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class AddDreamController implements Keys {

    @FXML
    private JFXButton addDreamButton;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private JFXTextArea dream;

    @FXML
    private JFXTimePicker timeOfAddDream;

    @FXML
    private JFXDatePicker dateOfAddDream;

    @FXML
    private JFXChipView<String> dreamTypeList;

    @FXML
    private JFXChipView<String> dreamHourList;

    @FXML
    private Pane textPane;

    private LocalDate currentDate;
    private LocalTime currentTime;
    private Time time = new Time();
    private Calendar calendar = new Calendar();

    private User user;
    private DatabaseHandler databaseHandler = new DatabaseHandler();

    @FXML
    private void initialize() {
        new Pulse(anchorPane).play();
        new BounceInDown(textPane).play();
        databaseHandler.connecting();
        user = BinderUser.getInstance().getUser();

        if (hasUnknownUser()) {
            return;
        }

        List<Integer> timeList = time.getCurrentTime();
        currentTime = LocalTime.of(timeList.get(0), timeList.get(1), timeList.get(2));
        List<Integer> dateList = calendar.getDate();
        currentDate = LocalDate.of(dateList.get(0), dateList.get(1), dateList.get(2));
        timeOfAddDream.setValue(currentTime);
        timeOfAddDream.set24HourView(true);
        dateOfAddDream.setValue(currentDate);

        DreamTypeHandler dreamTypeHandler = new DreamTypeHandler(databaseHandler.getConnection());
        List<DreamType> dreamTypeList = dreamTypeHandler.getDreamTypeList();
        for (DreamType dreamType : dreamTypeList) {
            this.dreamTypeList.getSuggestions().addAll(dreamType.getDreamType());
        }

        DreamHourHandler dreamHourHandler = new DreamHourHandler(databaseHandler.getConnection());
        List<DreamHour> dreamHourList = dreamHourHandler.getDreamHourList();
        for (DreamHour dreamHour : dreamHourList) {
            this.dreamHourList.getSuggestions().addAll(dreamHour.getDreamHour());
        }
    }

    public void addDream() {
        dreamHourList.autosize();
        dreamTypeList.autosize();
        AddDreamErrorsHandler addDreamErrorsHandler = new AddDreamErrorsHandler(dream, this.dreamTypeList, dreamHourList);
        ArrayList errorsListEmptyField = addDreamErrorsHandler.getErrorsEmptyFields();
        String errorMaxSize = addDreamErrorsHandler.getErrorsMaxSize();

        PutErrorsInDialog putErrorsInDialog = new PutErrorsInDialog(anchorPane);
        if (!errorsListEmptyField.isEmpty()) {
            putErrorsInDialog.setErrorList(errorsListEmptyField);
            putErrorsInDialog.showErrors(new Label("Предупреждение"));
        }

        if (errorMaxSize != null) {
            putErrorsInDialog.setError(errorMaxSize);
            putErrorsInDialog.showError(new Label("Предупреждение"));
        }

        if (addDreamErrorsHandler.isEmptyField(dream.getText()) | addDreamErrorsHandler.isEmptyObservableList(dreamTypeList.getChips())
                | addDreamErrorsHandler.isEmptyObservableList(dreamHourList.getChips()) | addDreamErrorsHandler.isMaxSize(dreamHourList.getChips())) {
            return;
        }

        ConfirmDialog confirmDialog = new ConfirmDialog(
                new Label("Подтверждение добавления сна"),
                new Text("Вы действительно собираетесь записать новый сон?\n"),
                anchorPane);
        confirmDialog.showDialog();

        confirmDialog.getAcceptButton().setOnAction(event -> {
            if (dateOfAddDream.getValue() == null) {
                dateOfAddDream.setValue(currentDate);
            }
            if (timeOfAddDream.getValue() == null) {
                timeOfAddDream.setValue(currentTime);
            }
            String dreamHour = dreamHourList.getChips().toString().replaceAll("[\\[\\](){}]","");

            DreamerHandler dreamerHandler  = new DreamerHandler(databaseHandler.getConnection(), user);
            Dream dream = new Dream(user.getId(), this.dream.getText(), dreamTypeList.getChips().toString(),
                    dreamHour, dateOfAddDream.getValue(), timeOfAddDream.getValue(), dreamerHandler.getCountOfEntries()+1);
            dreamerHandler.addDream(dream);
            new InfoDialog(new Label("Успешно"), new Text("Сон успешно записан"), anchorPane).showDialog();
            confirmDialog.closeDialog();
            new ChangerContainer(anchorPane, getClass().getResource(FXML_MAIN_CONTENT_RIGHT)).setLocation(1000);
        });
        confirmDialog.getCloseButton().setOnAction(event -> confirmDialog.closeDialog());

        databaseHandler.close();
    }

    private boolean hasUnknownUser() {
        if (BinderUser.getInstance().isUnknownBinderUser()) {
            addDreamButton.setDisable(true);
            BinderContainer.getInstance().setAnchor(anchorPane);
            new InfoDialog(
                    new Label("Ошибка!"), new Text("Неизвестный пользователь. Пожалуйста, авторизируйтесь."), BinderContainer.getInstance().getAnchor()).showDialogWithTimer();
            return true;
        }
        BinderContainer.getInstance().resetAnchorPane();
        return false;
    }
}