package dreams.controllers.logged;

import animatefx.animation.BounceInDown;
import animatefx.animation.Pulse;
import dreams.utils.BinderUser;
import dreams.utils.ChangerContainer;
import dreams.utils.Keys;
import dreams.utils.Time;
import dreams.database.DatabaseHandler;
import dreams.database.DreamerHandler;
import dreams.errors.PutErrorsInDialog;
import dreams.model.User;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

public class MainControllerController implements Keys {

    @FXML
    private AnchorPane rightColumnAnchorPane;

    @FXML
    private Pane textPane;

    @FXML
    private Pane functionsPane;

    @FXML
    private Button addMain;

    @FXML
    private Button showMain;

    @FXML
    private Button deleteMain;

    @FXML
    private Label welcomeLabel;

    @FXML
    private Label timeCounter;

    @FXML
    private Label accountNameLabel;

    @FXML
    private Label dreamsCounter;

    private User user;

    private DatabaseHandler databaseHandler = new DatabaseHandler();
    private DreamerHandler dreamerHandler;

    @FXML
    private void initialize(){
        user = BinderUser.getInstance().getUser();

        new Pulse(rightColumnAnchorPane).play();
        new BounceInDown(welcomeLabel).play();
        new BounceInDown(textPane).play();
        new BounceInDown(accountNameLabel).play();
        new BounceInDown(functionsPane).play();
        new BounceInDown(timeCounter).play();

        if (hasUnknownUser()) {
            return;
        }

        databaseHandler.connecting();
        dreamerHandler = new DreamerHandler(databaseHandler.getConnection(), this.user);
        Time timer = new Time(timeCounter);
        timer.getTimer();
        dreamsCounter.setText(String.valueOf(dreamerHandler.getCountOfEntries()));
        accountNameLabel.setText(user.getLogin());
    }

    @FXML
    private void handleAddDreamWindow() {
        new ChangerContainer(rightColumnAnchorPane, getClass().getResource(FXML_ADD_DREAM)).setLocation(500);
    }

    @FXML
    private void handleShowDream() {
        if (dreamerHandler.getCountOfEntries() < 1) {
            PutErrorsInDialog putErrorsInDialog = new PutErrorsInDialog(rightColumnAnchorPane);
            putErrorsInDialog.setError(" В таблице пока не содержится ни одного сноведения\n " +
                    "Перейдите на окно добавления сна, чтобы записать сноведение.");
            putErrorsInDialog.showError(new Label("Предупреждение"));
        } else new ChangerContainer(rightColumnAnchorPane, getClass().getResource(FXML_SHOW_DREAM)).setLocation(500);
        databaseHandler.close();
    }

    @FXML
    private void handleDeleteDream() {
        dreamerHandler = new DreamerHandler(databaseHandler.getConnection(), this.user);
        if (dreamerHandler.getCountOfEntries() < 1) {
            PutErrorsInDialog putErrorsInDialog = new PutErrorsInDialog(rightColumnAnchorPane);
            putErrorsInDialog.setError(" В таблице пока не содержится ни одного сноведения\n " +
                    "Перейдите на окно добавления сна, чтобы записать сноведение.");
            putErrorsInDialog.showError(new Label("Предупреждение"));
        } else new ChangerContainer(rightColumnAnchorPane, getClass().getResource(FXML_DELETE_DREAM)).setLocation(500);
        databaseHandler.close();
    }

    private boolean hasUnknownUser() {
        if (BinderUser.getInstance().isUnknownBinderUser()) {
            addMain.setDisable(true);
            showMain.setDisable(true);
            deleteMain.setDisable(true);
            accountNameLabel.setText("Неизвестный");
            return true;
        }
        return false;
    }
}