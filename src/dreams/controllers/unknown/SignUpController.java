package dreams.controllers.unknown;

import animatefx.animation.BounceInDown;
import animatefx.animation.Pulse;
import animatefx.animation.RubberBand;
import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;
import dreams.utils.Keys;
import dreams.utils.ChangerContainer;
import dreams.alerts.InfoDialog;
import dreams.errors.PutErrorsInDialog;
import dreams.alerts.ConfirmDialog;
import dreams.database.DatabaseHandler;
import dreams.database.UserHandler;
import dreams.errors.AuthorizationErrorsHandler;
import dreams.errors.RegistrationErrorsHandler;
import dreams.model.User;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class SignUpController implements Keys {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private JFXTextField userLogin;

    @FXML
    private JFXPasswordField userPassword;

    @FXML
    private Pane textPane;

    @FXML
    private MaterialIconView homeIcon;

    private DatabaseHandler databaseHandler = new DatabaseHandler();

    @FXML
    private void initialize(){
        new Pulse(anchorPane).play();
        new BounceInDown(textPane).play();
        databaseHandler.connecting();
    }

    @FXML
    private void handleClickBtnInputUser() {
        AuthorizationErrorsHandler authorizationErrorsHandler = new AuthorizationErrorsHandler(userLogin, userPassword);
        ArrayList errorsListEmptyField = authorizationErrorsHandler.getErrorsEmptyInputFields();
        ArrayList errorsListValidationField = authorizationErrorsHandler.getErrorsValidationFields();

        PutErrorsInDialog putErrorsInDialog = new PutErrorsInDialog(anchorPane);

        if (!errorsListEmptyField.isEmpty()) {
            putErrorsInDialog.setErrorList(errorsListEmptyField);
            putErrorsInDialog.showErrors(new Label("Предупреждение"));
        }

        if (!errorsListValidationField.isEmpty()) {
            putErrorsInDialog.setErrorList(errorsListValidationField);
            putErrorsInDialog.showErrors(new Label("Предупреждение"));
        }

        if (authorizationErrorsHandler.isEmptyField(userLogin.getText()) | authorizationErrorsHandler.isEmptyField(userPassword.getText())
                | authorizationErrorsHandler.isValidationPassword() | authorizationErrorsHandler.isValidationLogin()) {
            return;
        }

        User user = new User(userLogin.getText().trim(), userPassword.getText().trim());

        RegistrationErrorsHandler registrationErrorsHandler = new RegistrationErrorsHandler(databaseHandler.getConnection(), user);

        if (registrationErrorsHandler.isExistLogin()) {
            putErrorsInDialog.setError("Имя пользователя уже занято");
            putErrorsInDialog.showError(new Label("Ошибка регистрации"));
            return;
        }

        UserHandler registrarUser = new UserHandler(databaseHandler.getConnection(), user);
        ConfirmDialog confirmDialog = new ConfirmDialog(
                new Label("Подтверждение регистрации"),
                new Text("Вы действительно собираетесь создать нового пользователя\n" + user.getLogin() + "?"),
                anchorPane);
        confirmDialog.showDialog();

        confirmDialog.getAcceptButton().setOnAction(event -> {
            registrarUser.addUser();
            new InfoDialog(new Label("Успешно"), new Text("Регистрация успешно завершена"), anchorPane).showDialog();
            new ChangerContainer(anchorPane, getClass().getResource(FXML_LAUNCH)).setLocation(1000);
            confirmDialog.closeDialog();
        });
        confirmDialog.getCloseButton().setOnAction(event -> confirmDialog.closeDialog());

        databaseHandler.close();
    }

    @FXML
    private void handleLoginWindow() {
        new ChangerContainer(anchorPane, getClass().getResource(FXML_LOGIN)).setLocation(500);
    }

    @FXML
    private void handleLaunchWindow() {
        new RubberBand(homeIcon).play();
        new ChangerContainer(anchorPane, getClass().getResource(FXML_LAUNCH)).setLocation(500);
    }
}