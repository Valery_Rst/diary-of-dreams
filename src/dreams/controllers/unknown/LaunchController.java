package dreams.controllers.unknown;

import animatefx.animation.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import dreams.utils.BinderContainer;
import dreams.utils.Keys;
import dreams.utils.ChangerContainer;
import dreams.utils.BinderUser;
import javafx.fxml.FXML;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class LaunchController implements Keys {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private VBox vBox;

    @FXML
    private VBox sidebar;

    @FXML
    private Pane textPane;

    @FXML
    private FontAwesomeIconView openSidebar;

    private boolean visible = false;
    private String glyphName;

    @FXML
    private void initialize() {
        sidebar.setVisible(visible);
        new BounceInDown(textPane).play();
        glyphName = openSidebar.getGlyphName();
        openSidebar.setOnMouseClicked(event ->  makeVisible());

        BinderUser.getInstance().setUnknownBinderUser();
        BinderContainer.getInstance().setAnchor(anchorPane);
    }

    @FXML
    private void handleLoginWindow() {
        new ChangerContainer(anchorPane, getClass().getResource(FXML_LOGIN)).setLocation(500);
    }

    @FXML
    private void handleLaunchWindow() {
        new ChangerContainer(anchorPane, getClass().getResource(FXML_LAUNCH)).setLocation(500);
    }

    @FXML
    private void handleSignUpWindow() {
        new ChangerContainer(anchorPane, getClass().getResource(FXML_SIGN_UP)).setLocation(500);
    }

    @FXML
    private void handleAboutWindow() {
        new ChangerContainer(anchorPane, getClass().getResource(FXML_ABOUT)).setLocation(500);
    }

    @FXML
    private void handleAdditionallyWindow() {
        new ChangerContainer(anchorPane, getClass().getResource(FXML_ADDITIONALLY)).setLocation(500);
    }

    private void makeVisible() {
        if (!visible) {
            new RubberBand(openSidebar).play();
            openSidebar.setGlyphName("REMOVE");
            vBox.setEffect(new GaussianBlur());
            new FlipInX(sidebar).play();
            sidebar.setVisible(true);
            visible = true;
        } else {
            openSidebar.setGlyphName(glyphName);
            vBox.setEffect(null);
            new FlipOutX(sidebar).play();
            visible = false;
        }
    }
}