package dreams.controllers.unknown;

import animatefx.animation.BounceInDown;
import animatefx.animation.Pulse;
import animatefx.animation.RubberBand;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;
import dreams.utils.ChangerContainer;
import dreams.alerts.ConfirmDialog;
import dreams.alerts.InfoDialog;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.awt.*;
import java.net.URI;
import java.net.URISyntaxException;

public class AdditionallyController {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private Pane textPanel;

    @FXML
    private MaterialIconView homeIcon;

    private URI uri;
    private Desktop desktop;

    @FXML
    private void initialize() {
        new Pulse(anchorPane).play();
        new BounceInDown(textPanel).play();
    }

    public void handleLaunchWindow() {
        new RubberBand(homeIcon).play();
        new ChangerContainer(anchorPane, getClass().getResource("/views/unknown/launch.fxml")).setLocation(500);
    }

    public void resourceDairyDream() {
        setConnectWithBrowse("http://psymanblog.ru/meditacii/dnevnik-snovidenij-texniki-vospominaniya-snovidenij/");
    }

    public void resourceCreateDream() {
        setConnectWithBrowse("https://sna-kantata.ru/vidi-snovideniy/");
    }

    public void resourceDreamTypes() {
        setConnectWithBrowse("https://sna-kantata.ru/vidi-snovideniy/");
    }

    private void setConnectWithBrowse(String url) {
        try {
            uri = new URI(url);
        } catch (URISyntaxException e) {
            new InfoDialog(new Label("Ошибка"), new Text("К сожалению, запрашиваемый ресурс не найден"), anchorPane).showDialog();
            e.printStackTrace();
        }
        ConfirmDialog confirmDialog = new ConfirmDialog(new Label("Подтверждение"), new Text("Вы собираетесь перейти в браузер?"), anchorPane);
        confirmDialog.showDialog();
        confirmDialog.getAcceptButton().setOnAction(event -> {
            if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                try {
                    desktop.browse(uri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            confirmDialog.closeDialog();
        });
        confirmDialog.getCloseButton().setOnAction(event -> confirmDialog.closeDialog());
        desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
    }
}