package dreams.controllers.unknown;

import animatefx.animation.*;
import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;
import dreams.utils.Keys;
import dreams.utils.ChangerContainer;
import dreams.alerts.InfoDialog;
import dreams.database.UserHandler;
import dreams.errors.PutErrorsInDialog;
import dreams.database.DatabaseHandler;
import dreams.errors.AuthorizationErrorsHandler;
import dreams.utils.BinderUser;
import dreams.model.User;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class LoginController implements Keys {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private JFXTextField userLogin;

    @FXML
    private JFXPasswordField userPassword;

    @FXML
    private Pane textPane;

    @FXML
    private MaterialIconView homeIcon;

    private DatabaseHandler databaseHandler = new DatabaseHandler();

    @FXML
    private void initialize(){
        new Pulse(anchorPane).play();
        new BounceInDown(textPane).play();
        databaseHandler.connecting();
        databaseHandler.setAnchorPane(anchorPane);
    }

    @FXML
    private void handleClickBtnShowData() {
        AuthorizationErrorsHandler authorizationErrorsHandler = new AuthorizationErrorsHandler(userLogin, userPassword);
        ArrayList errorsListEmptyField = authorizationErrorsHandler.getErrorsEmptyInputFields();

        PutErrorsInDialog putErrorsInDialog = new PutErrorsInDialog(anchorPane);

        putErrorsInDialog.setErrorList(errorsListEmptyField);
        putErrorsInDialog.showErrors(new Label("Предупреждение"));

        if (authorizationErrorsHandler.isEmptyField(userLogin.getText()) | authorizationErrorsHandler.isEmptyField(userPassword.getText())) {
            return;
        }

        User user = new User(userLogin.getText().trim(), userPassword.getText().trim());

        BinderUser.getInstance().setUser(user);
        UserHandler finderLoggedUser = new UserHandler(databaseHandler.getConnection(), BinderUser.getInstance().getUser());
        BinderUser.getInstance().getUser().setId(finderLoggedUser.getIdLoggedUser());
        if (BinderUser.getInstance().getUser().getId() > 0) {
            new InfoDialog(new Label("Успешно"), new Text("Авторизация прошла успешно"), anchorPane).showDialog();
            new ChangerContainer(anchorPane, getClass().getResource(FXML_MAIN_CONTENT)).setLocation(1000);
        } else {
            putErrorsInDialog.setError("Неверный логин и/или пароль, или профиль не существует");
            putErrorsInDialog.showError(new Label("Ошибка доступа"));
        }

        databaseHandler.close();
    }

    @FXML
    private void handleSignUpWindow() {
        new ChangerContainer(anchorPane, getClass().getResource(FXML_SIGN_UP)).setLocation(500);
    }

    @FXML
    private void handleLaunchWindow() {
        new RubberBand(homeIcon).play();
        new ChangerContainer(anchorPane, getClass().getResource(FXML_LAUNCH)).setLocation(500);
    }
}