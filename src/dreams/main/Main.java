package dreams.main;

import animatefx.animation.FadeIn;
import com.jfoenix.controls.JFXDecorator;
import dreams.utils.Keys;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.IOException;

public class Main extends Application implements Keys {

    @Override
    public void start(Stage stage) throws IOException {
        AnchorPane anchorPane = FXMLLoader.load(getClass().getResource(FXML_LAUNCH));

        JFXDecorator decorator = new JFXDecorator(stage, anchorPane, false, false, true);
        decorator.setCustomMaximize(true);
        decorator.getStylesheets().add(getClass().getResource("/css/style.css").toExternalForm());
        decorator.setGraphic(new ImageView(new Image(new FileInputStream("resources/icons/Dairy_icon.png"))));
        decorator.setTitle("Diary of dreams");
        new FadeIn(decorator).play();
        stage.setScene(new Scene(decorator, 1024, 768));
        stage.getIcons().add(new Image("file:resources/icons/Dairy_icon.png"));
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args){
        launch(args);
    }
}