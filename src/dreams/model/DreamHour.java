package dreams.model;

public class DreamHour {

    private int id;
    private String dreamHour;

    public DreamHour(int id, String dreamHour) {
        this.id = id;
        this.dreamHour = dreamHour;
    }

    public int getId() {
        return id;
    }

    public String getDreamHour() {
        return this.dreamHour;
    }
}