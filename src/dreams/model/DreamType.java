package dreams.model;

public class DreamType {

    private int id;
    private String dreamType;

    public DreamType(int id, String dreamType) {
        this.id = id;
        this.dreamType = dreamType;
    }

    public int getId() {
        return id;
    }

    public String getDreamType() {
        return this.dreamType;
    }
}