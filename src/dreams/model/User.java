package dreams.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

public class User extends RecursiveTreeObject<User> {

    private String login, password;
    private int id;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return this.login;
    }

    public String getPassword() {
        return this.password;
    }

    public int getId() {
        return this.id;
    }
}