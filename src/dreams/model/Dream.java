package dreams.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;
import java.time.LocalTime;

public class Dream extends RecursiveTreeObject<Dream> {

    private int userId, countEntries;
    private String dream, dreamType, dreamHour;
    private LocalDate date;
    private LocalTime time;

    public StringProperty numberProperty;
    public StringProperty dreamProperty;
    public StringProperty dreamTypeProperty;
    public StringProperty dreamHourProperty;
    public ObjectProperty<LocalDate> dateProperty;
    public ObjectProperty<LocalTime> timeProperty;

    public Dream(int userId, String dream, String dreamType, String dreamHour, LocalDate date, LocalTime time, int countEntries) {
        this.userId = userId;
        this.dream = dream;
        this.dreamType = dreamType;
        this.dreamHour = dreamHour;
        this.date = date;
        this.time = time;
        this.countEntries = countEntries;

        this.numberProperty = new SimpleStringProperty(String.valueOf(this.countEntries));
        this.dreamProperty = new SimpleStringProperty(dream);
        this.dreamTypeProperty = new SimpleStringProperty(dreamType);
        this.dreamHourProperty = new SimpleStringProperty(dreamHour);
        this.dateProperty = new SimpleObjectProperty<>(date);
        this.timeProperty = new SimpleObjectProperty<>(time);
    }

    public int getUserId() {
        return this.userId;
    }

    public int getCountEntries() {
        return this.countEntries;
    }

    public String getDream() {
        return this.dream;
    }

    public String getDreamType() {
        return this.dreamType;
    }

    public String getDreamHour() {
        return this.dreamHour;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public LocalTime getTime() {
        return this.time;
    }
}