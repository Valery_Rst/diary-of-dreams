package dreams.database;

import dreams.model.User;
import dreams.utils.Encoder;

import java.sql.*;
import java.util.Objects;

public class UserHandler {

    private Connection connection;
    private User user;

    public UserHandler(Connection connection, User user) {
        this.connection = connection;
        this.user = user;
    }

    public void addUser() {
        try (PreparedStatement statement = this.connection.prepareStatement(
                "INSERT INTO Users(`login`, `password`) VALUES(?, ?)")) {
            statement.setObject(1, user.getLogin());
            statement.setObject(2, Encoder.getEncryptedPasswordWithMD5(user.getPassword()));
            statement.execute();
        } catch (SQLException e) {
            //ERROR
            e.printStackTrace();
        }
    }

    public int getIdLoggedUser() {
        try (Statement statement = this.connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT id, login, password FROM Users");
            while (resultSet.next()) {
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                if (user.getLogin().equals(login) && Objects.equals(Encoder.getEncryptedPasswordWithMD5(user.getPassword()), password)) {
                    return resultSet.getInt("id");
                }
            }
        } catch (SQLException e) {
            //ERROR
            e.printStackTrace();
        }
        return 0;
    }

    public void deleteUser() {
        try (PreparedStatement statement = this.connection.prepareStatement(
                "DELETE FROM Users WHERE id = ?")) {
            statement.setObject(1, user.getId());
            statement.execute();
        } catch (SQLException e) {
            //ERROR
            e.printStackTrace();
        }
    }
}