package dreams.database;

import dreams.model.Dream;
import dreams.model.User;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DreamerHandler {

    private Connection connection;
    private User user;

    public DreamerHandler(Connection connection, User user) {
        this.connection = connection;
        this.user = user;
    }

    public int getCountOfEntries() {
        int count = 0;
        try(Statement statement = this.connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT count(*) AS COUNT from Dreams WHERE user_id=" + user.getId());
            resultSet.next();
            count = resultSet.getInt("COUNT");
        } catch (SQLException e) {
            //ERROR
            e.printStackTrace();
        }
        return count;
    }

    public void addDream(Dream dream) {
        try (PreparedStatement preparedStmt = this.connection.prepareStatement
                ("INSERT INTO 'Dreams' ('user_id', 'dream', 'dream_type', 'dream_hour', 'date_entry', 'time_entry', 'count_entries') VALUES(?, ?, ?, ?, ?, ?, ?)")){
                preparedStmt.setObject(1, dream.getUserId());
                preparedStmt.setObject(2, dream.getDream());
                preparedStmt.setObject(3, dream.getDreamType());
                preparedStmt.setObject(4, dream.getDreamHour());
                preparedStmt.setObject(5, dream.getDate());
                preparedStmt.setObject(6, dream.getTime());
                preparedStmt.setObject(7, dream.getCountEntries());
                preparedStmt.execute();
        } catch (SQLException e) {
            //ERROR
            e.printStackTrace();
        }
    }

    public void updateDream(Dream dream) {
        try (PreparedStatement preparedStmt = this.connection.prepareStatement
                ("UPDATE Dreams SET count_entries =?, dream =? , dream_type =? , dream_hour =? WHERE id =?" )){
            preparedStmt.setObject(1, dream.getCountEntries());
            preparedStmt.setObject(2, dream.getDream());
            preparedStmt.setObject(3, dream.getDreamType());
            preparedStmt.setObject(4, dream.getDreamHour());
            preparedStmt.setObject(5, dream.getUserId());
            preparedStmt.execute();
        } catch (SQLException e) {
            //ERROR
            e.printStackTrace();
        }
    }

    public List<Dream> getUserDreamsList() {
        try (Statement statement = this.connection.createStatement()) {
            List<Dream> dreamsUserList = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT id, user_id, dream, dream_type, dream_hour, date_entry, time_entry, count_entries FROM Dreams WHERE user_id=" + user.getId());
            while (resultSet.next()) {
                LocalDate date = LocalDate.parse(resultSet.getString("date_entry"));
                LocalTime time = LocalTime.parse(resultSet.getString("time_entry"));
                dreamsUserList.add(new Dream(
                        resultSet.getInt("id"),
                        resultSet.getString("dream"),
                        resultSet.getString("dream_type"),
                        resultSet.getString("dream_hour"),
                        date,
                        time,
                        resultSet.getInt("count_entries")));
            }
            return dreamsUserList;
        } catch (SQLException e) {
            //ERROR
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public void deleteAllUserDreams() {
        try (PreparedStatement statement = this.connection.prepareStatement(
                "DELETE FROM Dreams WHERE user_id = ?")) {
            statement.setObject(1, user.getId());
            statement.execute();
        } catch (SQLException e) {
            //ERROR
            e.printStackTrace();
        }
    }

    public void deleteSelectUserDream(Dream dream) {
        try (PreparedStatement statement = this.connection.prepareStatement(
                "DELETE FROM Dreams WHERE id = ?")) {
            statement.setObject(1, dream.getUserId());
            statement.execute();
        } catch (SQLException e) {
            //ERROR
            e.printStackTrace();
        }
    }
}