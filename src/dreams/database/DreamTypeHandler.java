package dreams.database;

import dreams.model.DreamType;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DreamTypeHandler {

    private Connection connection;

    public DreamTypeHandler(Connection connection) {
        this.connection = connection;
    }

    public List<DreamType> getDreamTypeList() {
        try (Statement statement = this.connection.createStatement()) {
            List<DreamType> dreamTypes = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery("SELECT id, type FROM Types_of_dream");
            while (resultSet.next()) {
                dreamTypes.add(new DreamType(resultSet.getInt("id"),
                        resultSet.getString("type")));
            }
            return dreamTypes;
        } catch (SQLException e) {
            //ERROR
            e.getStackTrace();
        }
        return Collections.emptyList();
    }
}