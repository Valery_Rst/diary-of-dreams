package dreams.database;

import dreams.utils.BinderContainer;
import dreams.utils.Keys;
import dreams.alerts.InfoDialog;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.sql.*;

public class DatabaseHandler implements Keys {

    private static final String DB_URL = "jdbc:sqlite:resources/db/DairyDream.s3db";
    private static final String DB_Driver = "org.sqlite.JDBC";

    private Connection connection;
    private AnchorPane anchorPane;

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }

    public void setAnchorPane(AnchorPane anchorPane) {
        this.anchorPane = anchorPane;
    }

    public  DatabaseHandler() {
    }

    public Connection getConnection() {
        try {
            return DriverManager.getConnection(DB_URL);
        } catch (SQLException e) {
            new InfoDialog(
                    new Label("Ошибка!"), new Text("Ошибка получения соединения с базой данных\n" +
                    "Пожалуйста, проверьте наличие базы данных и/или наличие библиотеки. Проверьте ра \n" +
                    "для работы с базой данных."), BinderContainer.getInstance().getAnchor()).showDialogWithTimer();
            e.printStackTrace();
        }
        return null;
    }

    public void connecting() {
        try {
            Class.forName(DB_Driver);
            this.connection = DriverManager.getConnection(DB_URL);
        } catch (ClassNotFoundException | SQLException e) {
            new InfoDialog(
                    new Label("Ошибка!"), new Text("Ошибка открытия соединения с базой данных\n" +
                    "Пожалуйста, проверьте наличие базы данных и/или наличие библиотеки \n" +
                    "для работы с базой данных."), BinderContainer.getInstance().getAnchor()).showDialogWithTimer();
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            new InfoDialog(
                    new Label("Ошибка!"), new Text("Ошибка закрытия соединения с базой данных\n" +
                    "Пожалуйста, проверьте наличие базы данных и/или наличие библиотеки \n" +
                    "для работы с базой данных"), BinderContainer.getInstance().getAnchor()).showDialogWithTimer();
            e.printStackTrace();
        }
    }
}