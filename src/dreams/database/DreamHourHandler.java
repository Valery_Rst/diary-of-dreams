package dreams.database;

import dreams.model.DreamHour;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DreamHourHandler {

    private Connection connection;

    public DreamHourHandler(Connection connection) {
        this.connection = connection;
    }

    public List<DreamHour> getDreamHourList() {
        try (Statement statement = this.connection.createStatement()) {
            List<DreamHour> dreamHour = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery("SELECT id, hour FROM Hours_of_dream");
            while (resultSet.next()) {
                dreamHour.add(new DreamHour(resultSet.getInt("id"),
                        resultSet.getString("hour")));
            }
            return dreamHour;
        } catch (SQLException e) {
            //ERROR
            e.getStackTrace();
        }
        return Collections.emptyList();
    }
}