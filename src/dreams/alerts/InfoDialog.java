package dreams.alerts;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import dreams.utils.ChangerContainer;
import dreams.utils.BinderContainer;
import dreams.utils.Keys;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

public class InfoDialog extends Dialog implements Keys {

    private JFXDialog dialog;
    private JFXButton closeButton;

    public InfoDialog(Label header, Text text, AnchorPane anchorPane) {
        super(header, text, anchorPane);
    }

    public void showDialog() {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(header);
        content.setBody(text);

        StackPane stackPane = new StackPane();
        stackPane.autosize();
        dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER, true);

        closeButton = new JFXButton("Ок");
        closeButton.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        closeButton.setPrefHeight(40);
        closeButton.setPrefWidth(100);
        closeButton.setStyle("-fx-background-color: #8245bf; -fx-text-fill: white;");
        closeButton.setOnAction(event -> closeDialog());

        int dialogWidth = 450;
        int dialogHeight = 250;
        if (anchorPane.getWidth() == 1024 && anchorPane.getHeight() == 768) {
            AnchorPane.setTopAnchor(stackPane, 180d);
            AnchorPane.setLeftAnchor(stackPane, 287d);
        } else if (anchorPane.getWidth() == 954 && anchorPane.getHeight() == 768) {
            AnchorPane.setTopAnchor(stackPane, 180d);
            AnchorPane.setLeftAnchor(stackPane, 187d);
        }
        content.setActions(closeButton);
        content.setPrefSize(dialogWidth, dialogHeight);

        anchorPane.getChildren().add(stackPane);
        dialog.show();
    }

    public void showDialogWithTimer() {
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        Platform.runLater(() -> {
                           showDialog();
                           closeButton.setOnAction(event -> {
                               closeDialog();
                               new ChangerContainer(BinderContainer.getInstance().getAnchor(), getClass().getResource(FXML_LAUNCH)).setLocation(500);
                           });
                        });
                    }}, 500);
    }


    public void closeDialog() {
        dialog.close();
    }
}