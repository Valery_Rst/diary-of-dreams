package dreams.alerts;

import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

public abstract class Dialog {

    Text text;
    Label header;
    AnchorPane anchorPane;

    Dialog(Label header, Text text, AnchorPane anchorPane) {
        this.header = header;
        this.text = text;
        this.anchorPane = anchorPane;
    }

    public abstract void showDialog();
    public abstract void closeDialog();
}