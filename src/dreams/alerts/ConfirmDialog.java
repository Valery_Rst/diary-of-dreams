package dreams.alerts;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

public class ConfirmDialog  extends Dialog{

    private JFXButton acceptButton, closeButton;
    private JFXDialog dialog;

    public ConfirmDialog(Label header, Text text, AnchorPane anchorPane) {
        super(header, text, anchorPane);
    }

    public JFXButton getAcceptButton() {
        return acceptButton;
    }

    public JFXButton getCloseButton() {
        return closeButton;
    }

    public void showDialog() {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(header);
        content.setBody(text);

        StackPane stackPane = new StackPane();
        stackPane.autosize();
        dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER, true);

        acceptButton = new JFXButton("Да");
        acceptButton.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        acceptButton.setStyle("-fx-background-color: #8245bf; -fx-text-fill: white;");
        acceptButton.setPrefHeight(40);
        acceptButton.setPrefWidth(100);

        closeButton = new JFXButton("Нет");
        closeButton.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        closeButton.setPrefHeight(40);
        closeButton.setPrefWidth(100);
        closeButton.setStyle("-fx-background-color: #8245bf; -fx-text-fill: white;");

        int dialogWidth = 450;
        int dialogHeight = 250;
        if (anchorPane.getWidth() == 1024 && anchorPane.getHeight() == 768) {
            AnchorPane.setTopAnchor(stackPane, 180d);
            AnchorPane.setLeftAnchor(stackPane, 287d);
        } else if (anchorPane.getWidth() == 954 && anchorPane.getHeight() == 768) {
            AnchorPane.setTopAnchor(stackPane, 180d);
            AnchorPane.setLeftAnchor(stackPane, 187d);
        }
        content.setPrefSize(dialogWidth, dialogHeight);
        content.setActions(acceptButton, closeButton);

        anchorPane.getChildren().add(stackPane);
        dialog.show();
    }

    public void closeDialog() {
        dialog.close();
    }
}