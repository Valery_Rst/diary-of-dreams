package dreams.errors;

import com.jfoenix.controls.JFXChipView;
import com.jfoenix.controls.JFXTextArea;
import javafx.collections.ObservableList;

import java.util.ArrayList;

public class AddDreamErrorsHandler {

    private String dreamText;
    private ObservableList<String> dreamTypeText;
    private ObservableList<String> dreamHourText;

    private JFXTextArea dream;
    private JFXChipView<String> dreamType;
    private JFXChipView<String> dreamHour;

    private ArrayList<String> errorsEmptyField = new ArrayList<>();

    public AddDreamErrorsHandler(JFXTextArea dream, JFXChipView<String> dreamType, JFXChipView<String> dreamHour) {
        this.dreamText = dream.getText();
        this.dreamTypeText = dreamType.getChips();
        this.dreamHourText = dreamHour.getChips();

        this.dream = dream;
        this.dreamType = dreamType;
        this.dreamHour = dreamHour;
    }

    public AddDreamErrorsHandler() {
    }

    public ArrayList getErrorsEmptyFields() {
        if (isEmptyField(dreamText)) {
            errorsEmptyField.add("Введите Ваш сон. \n\n");
            new animatefx.animation.Shake(dream).play();
        }
        if (isEmptyObservableList(dreamTypeText)) {
            errorsEmptyField.add("Выберите или введите типы (метки) сна. \n" +
                    "Убедитесь, что нажали клавишу вввода.\n\n");
            new animatefx.animation.Shake(dreamType).play();
        }
        if (isEmptyObservableList(dreamHourText)) {
            errorsEmptyField.add("Выберите или введите продолжительность сна.\n" +
                    "Убедитесь, что нажали клавишу вввода.\n\n");
            new animatefx.animation.Shake(dreamHour).play();
        }
        return errorsEmptyField;
    }

    public String getErrorsMaxSize() {
        if (!isEmptyObservableList(dreamHourText)) {
            if (isMaxSize(dreamHourText)) {
                new animatefx.animation.Shake(dreamHour).play();
                return "Продолжительность сна должно имееть максимум одно значение.";
            }
        }
        return null;
    }

    public boolean isMaxSize(ObservableList<String> field) {
        return field.size() > 1;
    }

    public boolean isEmptyField(String field) {
        return field.isEmpty();
    }

    public boolean isEmptyObservableList(ObservableList<String> field) {
        return field.isEmpty();
    }
}