package dreams.errors;

import dreams.model.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RegistrationErrorsHandler {

    private Connection connection;
    private User user;

    public RegistrationErrorsHandler(Connection connection, User user) {
        this.connection = connection;
        this.user = user;
    }

    public boolean isExistLogin() {
        try (Statement statement = this.connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT id, login FROM Users");
            while (resultSet.next()) {
                String login = resultSet.getString("login");
                if (this.user.getLogin().equals(login)) {
                    return true;
                }
            }
        } catch (SQLException e) {
            //ERROR
            e.printStackTrace();
        }
        return false;
    }
}