package dreams.errors;

import dreams.alerts.InfoDialog;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class PutErrorsInDialog {

    private ArrayList errorList;
    private String error;
    private AnchorPane anchorPane;

    public PutErrorsInDialog(AnchorPane anchorPane) {
        this.anchorPane = anchorPane;
    }

    public void setErrorList(ArrayList errorList) {
        this.errorList = errorList;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void showErrors(Label label) {
        StringBuilder separator = new StringBuilder();
        for (Object error : errorList) {
            separator.append(error);
        }

        if (!errorList.isEmpty()) {
            new InfoDialog(label, new Text(separator.toString()), anchorPane).showDialog();
        }
    }

    public void showError(Label label) {
        new InfoDialog(label, new Text(error), anchorPane).showDialog();
    }
}