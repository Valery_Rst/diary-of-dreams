package dreams.errors;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthorizationErrorsHandler {

    private JFXTextField jfxLogin;
    private JFXPasswordField jfxPassword;
    private String login, password;
    private ArrayList<String> errorsEmptyField = new ArrayList<>();
    private ArrayList<String> errorsValidationField = new ArrayList<>();

    public AuthorizationErrorsHandler(JFXTextField jfxLogin, JFXPasswordField jfxPassword) {
        this.login = jfxLogin.getText();
        this.password = jfxPassword.getText();
        this.jfxLogin = jfxLogin;
        this.jfxPassword = jfxPassword;
    }

    public ArrayList getErrorsEmptyInputFields() {
        if (isEmptyField(login)) {
            errorsEmptyField.add("Введите логин.\n\n");
            new animatefx.animation.Shake(jfxLogin).play();
        }
        if(isEmptyField(password)) {
            errorsEmptyField.add("Введите пароль.\n");
            new animatefx.animation.Shake(jfxPassword).play();
        }
        return errorsEmptyField;
    }

    public ArrayList getErrorsValidationFields() {
        if (!isEmptyField(login) && !isEmptyField(password)) {
            if (isValidationLogin()) {
                errorsValidationField.add("Логин должен содержать не менее 6 и не более 16 символов, " +
                        "\nиметь минимум 1 заглавную букву, 1 строчную букву, без \nцифр.\n\n");
                new animatefx.animation.Shake(jfxLogin).play();
            }
            if (isValidationPassword()) {
                errorsValidationField.add("Пароль должен содержать не менее шести и не более 16 символов, " +
                        "\n иметь минимум 1 заглавную букву, 1 строчную букву и 1 \nцифру.\n");
                new animatefx.animation.Shake(jfxPassword).play();
            }
        }
        return errorsValidationField;
    }

    public boolean isEmptyField(String field) {
        return field.isEmpty();
    }

    public boolean isValidationLogin() {
        Pattern pattern = Pattern.compile("^(?=.*[a-z|а-я])(?=.*[A-Z|А-Я])(?!.*[0-9]).{6,16}$");
        Matcher matcher = pattern.matcher(login);
        return !matcher.find();
    }

    public boolean isValidationPassword() {
        Pattern pattern = Pattern.compile("^(?=.*[a-z|а-я])(?=.*[A-Z|А-Я])(?=.*[0-9]).{6,20}$");
        Matcher matcher = pattern.matcher(password);
        return !matcher.find();
    }
}