package dreams.utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Calendar {

    public Calendar() {
    }

    public List<Integer> getDate() {
        List<Integer> date = new ArrayList<>();
        LocalDate currentDate = LocalDate.now();
        date.add(currentDate.getYear());
        date.add(currentDate.getMonthValue());
        date.add(currentDate.getDayOfMonth());
        return date;
    }
}