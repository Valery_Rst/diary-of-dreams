package dreams.utils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
public class Encoder {
    public static String getEncryptedPasswordWithMD5(String password){
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            byte[] passBytes = password.getBytes();
            messageDigest.reset();
            byte[] digested = messageDigest.digest(passBytes);
            StringBuilder stringBuilder = new StringBuilder();
            for (byte b : digested) {
                stringBuilder.append(Integer.toHexString(0xff & b));
            }
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException ex) {
            ex.getStackTrace();
        }
        return null;
    }
}