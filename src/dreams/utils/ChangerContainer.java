package dreams.utils;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;

public class ChangerContainer {

    private AnchorPane anchorPane;
    private URL location;

    public ChangerContainer(AnchorPane anchorPane, URL location) {
        this.anchorPane = anchorPane;
        this.location = location;
    }

    public void setLocation(long delay) {
        /* Platform.runLater is fix problem with UI (UI is updated from the parent thread)*/
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        Platform.runLater(() -> {
                            try {
                                anchorPane.getChildren().setAll((Node) FXMLLoader.load(location));
                            } catch (IOException e) {
                                e.getStackTrace();
                            }
                        });
                    }}, delay);
    }
}