package dreams.utils;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Time {

    private javafx.scene.control.Label timeCounter;

    public Time() {
    }

    public Time(javafx.scene.control.Label timeCounter) {
        this.timeCounter = timeCounter;
    }

    public void getTimer() {
        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            LocalTime currentTime = LocalTime.now();
            timeCounter.setText
                    (((currentTime.getHour() < 10)? "0" + currentTime.getHour() : currentTime.getHour()) + ":" +
                            ((currentTime.getMinute() < 10) ? "0" + currentTime.getMinute() : currentTime.getMinute()) + ":" +
                            ((currentTime.getSecond() < 10) ? "0" + currentTime.getSecond() : currentTime.getSecond()));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    public List<Integer> getCurrentTime() {
        List<Integer> time = new ArrayList<>();
        LocalTime currentTime = LocalTime.now();
        time.add(currentTime.getHour());
        time.add(currentTime.getMinute());
        time.add(currentTime.getSecond());

        return time;
    }
}