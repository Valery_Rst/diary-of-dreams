package dreams.utils;

import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.cells.editors.TextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell;
import dreams.database.DatabaseHandler;
import dreams.database.DreamerHandler;
import dreams.model.Dream;
import dreams.model.User;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class TreeTableColumns {

    private ObservableList<Dream> observableList = FXCollections.observableArrayList();
    private DatabaseHandler databaseHandler = new DatabaseHandler();

    private JFXTreeTableColumn<Dream, String> dreamNumber = new JFXTreeTableColumn<>("№");
    private JFXTreeTableColumn<Dream, String> dream = new JFXTreeTableColumn<>("Сон");
    private JFXTreeTableColumn<Dream, String> dreamType = new JFXTreeTableColumn<>("Тип сна");
    private JFXTreeTableColumn<Dream, String> dreamHour = new JFXTreeTableColumn<>("Часов сна");
    private JFXTreeTableColumn<Dream, LocalDate> dreamDate = new JFXTreeTableColumn<>("Дата");
    private JFXTreeTableColumn<Dream, LocalTime> dreamTime = new JFXTreeTableColumn<>("Время");

    private String editDream, editType, editHour;
    private int editNumber;

    public TreeTableColumns() {
    }

    private void setEditNumber(int editNumber) {
        this.editNumber = editNumber;
    }
    private void setEditDream(String editDream) {
        this.editDream = editDream;
    }

    private void setEditType(String editType) {
        this.editType = editType;
    }

    private void setEditHour(String editHour) {
        this.editHour = editHour;
    }

    public String getEditDream() {
        return editDream;
    }

    public String getEditType() {
        return editType;
    }

    public String getEditHour() {
        return editHour;
    }

    public int getEditNumber() {
        return editNumber;
    }

    public ObservableList<Dream> getObservableList() {
        return observableList;
    }

    public JFXTreeTableColumn<Dream, String> getDreamNumber() {
        return dreamNumber;
    }

    public JFXTreeTableColumn<Dream, String> getDream() {
        return dream;
    }

    public JFXTreeTableColumn<Dream, String> getDreamType() {
        return dreamType;
    }

    public JFXTreeTableColumn<Dream, String> getDreamHour() {
        return dreamHour;
    }

    public JFXTreeTableColumn<Dream, LocalDate> getDreamDate() {
        return dreamDate;
    }

    public JFXTreeTableColumn<Dream, LocalTime> getDreamTime() {
        return dreamTime;
    }

    public void setViewTableColumns() {
        dreamNumber.setCellValueFactory(param -> param.getValue().getValue().numberProperty);
        dream.setCellValueFactory(param -> param.getValue().getValue().dreamProperty);
        dreamType.setCellValueFactory(param -> param.getValue().getValue().dreamTypeProperty);
        dreamHour.setCellValueFactory(param -> param.getValue().getValue().dreamHourProperty);
        dreamDate.setCellValueFactory(param -> param.getValue().getValue().dateProperty);
        dreamTime.setCellValueFactory(param -> param.getValue().getValue().timeProperty);
    }

    public void setEditTableColumns() {
        dreamNumber.setCellFactory((TreeTableColumn<Dream, String> param) -> new GenericEditableTreeTableCell<>(
                new TextFieldEditorBuilder()));
        dreamNumber.setOnEditCommit((TreeTableColumn.CellEditEvent<Dream, String> t) -> {
            t.getTreeTableView().getTreeItem(t.getTreeTablePosition().getRow()).getValue().numberProperty.set(t.getNewValue());
            if (t.getNewValue() != null) {
                setEditNumber(Integer.parseInt(t.getNewValue()));
            }
        });
        dream.setCellFactory((TreeTableColumn<Dream, String> param) -> new GenericEditableTreeTableCell<>(
                new TextFieldEditorBuilder()));
        dream.setOnEditCommit((TreeTableColumn.CellEditEvent<Dream, String> t) -> {
            t.getTreeTableView().getTreeItem(t.getTreeTablePosition().getRow()).getValue().dreamProperty.set(t.getNewValue());
            if (t.getNewValue() != null) {
                setEditDream(t.getNewValue());
            }
        });
        dreamType.setCellFactory((TreeTableColumn<Dream, String> param) -> new GenericEditableTreeTableCell<>(
                new TextFieldEditorBuilder()));
        dreamType.setOnEditCommit((TreeTableColumn.CellEditEvent<Dream, String> t) -> {
            t.getTreeTableView().getTreeItem(t.getTreeTablePosition().getRow()).getValue().dreamTypeProperty.set(t.getNewValue());
            if (t.getNewValue() != null) {
                setEditType(t.getNewValue());
            }
        });
        dreamHour.setCellFactory((TreeTableColumn<Dream, String> param) -> new GenericEditableTreeTableCell<>(
                new TextFieldEditorBuilder()));
        dreamHour.setOnEditCommit((TreeTableColumn.CellEditEvent<Dream, String> t) -> {
            t.getTreeTableView().getTreeItem(t.getTreeTablePosition().getRow()).getValue().dreamHourProperty.set(t.getNewValue());
            if (t.getNewValue() != null) {
                setEditHour(t.getNewValue());
            }
        });
    }

    public String getValue(String prev, String current) {
        return prev.equals(current) || current == null ? prev : current;
    }

    public int getValue(int prev, int current) {
        return prev == current || current == 0 ? prev : current;
    }

    public void setSearchView(JFXTreeTableView<Dream> treeTable, AnchorPane anchorPane) {
        FlowPane main = new FlowPane();
        main.setAlignment(Pos.CENTER);
        main.getChildren().add(treeTable);

        JFXTextField filterField = new JFXTextField();
        filterField.setUnFocusColor(Color.valueOf("#8245BF"));
        filterField.setPromptText("Поиск");
        filterField.setPadding(new Insets(5));
        main.getChildren().add(filterField);

        Label size = new Label();
        filterField.textProperty().addListener((o, oldVal, newVal) -> treeTable.setPredicate(userProp -> {
            final Dream dreamer = userProp.getValue();
            return dreamer.dreamProperty.get().contains(newVal)
                    || dreamer.dreamTypeProperty.get().contains(newVal)
                    || dreamer.dreamHourProperty.get().contains(newVal)
                    || dreamer.numberProperty.get().contains(newVal);
        }));
        size.textProperty()
                .bind(Bindings.createStringBinding(() -> String.valueOf(treeTable.getCurrentItemsCount()),
                        treeTable.currentItemsCountProperty()));
        main.getChildren().add(size);
        anchorPane.getChildren().addAll(main);
    }

    public void initData() {
        User user = BinderUser.getInstance().getUser();
        databaseHandler.connecting();
        DreamerHandler dreamerHandler = new DreamerHandler(databaseHandler.getConnection(), user);
        List<Dream> dreamerHandlerList = dreamerHandler.getUserDreamsList();
        for (Dream dreamer : dreamerHandlerList) {
            observableList.addAll(dreamer);
        }
    }

    public void refresh() {
        observableList.clear();
        initData();
    }
}