package dreams.utils;

public interface Keys {
    String FXML_LAUNCH = "/views/unknown/launch.fxml";
    String FXML_LOGIN= "/views/unknown/login.fxml";
    String FXML_SIGN_UP = "/views/unknown/sign_up.fxml";
    String FXML_ABOUT = "/views/unknown/about.fxml";
    String FXML_ADDITIONALLY = "/views/unknown/additionally.fxml";

    String FXML_MAIN_CONTENT = "/views/logged/main-content_sidebar.fxml";
    String FXML_MAIN_CONTENT_RIGHT = "/views/logged/main-content.fxml";
    String FXML_ADD_DREAM = "/views/logged/add_dream.fxml";
    String FXML_SHOW_DREAM = "/views/logged/show_dream.fxml";
    String FXML_DELETE_DREAM = "/views/logged/delete_dream.fxml";
}