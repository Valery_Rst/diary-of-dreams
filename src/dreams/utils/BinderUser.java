package dreams.utils;

import dreams.model.User;

public class BinderUser {

    private static BinderUser instance;
    private User user;

    public static BinderUser getInstance() {
        if(instance == null) {
            instance = new BinderUser();
        }
        return instance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUnknownBinderUser() {
        user = null;
    }

    public boolean isUnknownBinderUser() {
        return user == null;
    }
}