package dreams.utils;

import javafx.scene.layout.AnchorPane;

public class BinderContainer {

    private static BinderContainer instance;
    private AnchorPane anchorPane;

    public static BinderContainer getInstance() {
        if(instance == null) {
            instance = new BinderContainer();
        }
        return instance;
    }

    public AnchorPane getAnchor() {
        return anchorPane;
    }

    public void setAnchor(AnchorPane anchorPane) {
        this.anchorPane = anchorPane;
    }

    public void resetAnchorPane() {
        anchorPane = null;
    }
}